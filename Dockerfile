# Start writing your Dockerfile easily
FROM ccrisan/motioneye:master-amd64
ADD https://gist.githubusercontent.com/golfcomsci/d31bbe20efc6bcfc8f72937f1a952287/raw/1cef4a61fd7a790f77078df2ab3e4393fc422bb5/telegram.sh /usr/local/bin
RUN chmod 777 /usr/local/bin/telegram.sh
ADD https://gist.githubusercontent.com/golfcomsci/d31bbe20efc6bcfc8f72937f1a952287/raw/1cef4a61fd7a790f77078df2ab3e4393fc422bb5/.telegram.sh /etc/telegram.sh.conf
